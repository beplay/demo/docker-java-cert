FROM openjdk:8-jre-alpine

COPY ./app/* /app/
COPY ./certs/* /usr/local/share/ca-certificates/

RUN \
  # Update certificate
  update-ca-certificates && \
  \
  # Add nonroot user
  addgroup -S appgroup && \
  adduser -S appadm -G appgroup && \
  chown -R appadm:appgroup /app

WORKDIR /app
USER appadm